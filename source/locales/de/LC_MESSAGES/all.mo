��    0      �  C         (     )     /     <     K     [     a     h     p     w     �     �     �     �     �     �  -   �                    "  	   /     9     A     N  	   ^  
   h     s     v     {     �     �  
   �     �     �     �  	   �     �     �               !     .     5     A     H     ^     m  9  �     �     �     �     �          %     +  	   7     A     \     v          �  %   �     �  9   �     	     '	     6	     ;	     L	     c	     y	     �	     �	     �	     �	     �	     �	  !   �	     

  
   
  +   &
     R
     r
     �
     �
     �
     �
     �
     �
     �
     �
     �
          $  .   <         -   &   !   '      $              %                     	                                  )   /   0                   +             
                           ,   .                        #      (   *             "                 About Add to Queue Add to library Add to playlist Added Albums Artists Cancel Community Playlists Could not Rename Delete Delete Playlist? Donate Download to Music Folder Enter Name... Find songs to play using the search bar above Go back Loop More More actions Move Down Move Up New Playlist New Playlist... Next song No Results Ok Play Play All Playlist already exists Previous song Radio Mode Remove From Downloads Remove From Queue Rename Playlist Rename... Saved Search for Content... Shuffle Songs Toggle pause Videos View Artist Volume Your Library is Empty Your Playlists translator-credits Project-Id-Version: 
PO-Revision-Date: 
Last-Translator: Jürgen Benvenuti <gastornis@posteo.org>
Language-Team: German <gnome-de@gnome.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.1.1
 Info Zur Warteschlange hinzufügen Zur Bibliothek hinzufügen Zur Wiedergabeliste hinzufügen Hinzugefügt Alben Interpreten Abbrechen Community-Wiedergabelisten Umbenennen nicht möglich Löschen Wiedergabeliste löschen? Spenden In den »Musik«-Ordner herunterladen Einen Namen eingeben … Suchen Sie in der Leiste oben nach Titeln zum Wiedergeben Zurückgehen Endlosschleife Mehr Weitere Aktionen Nach unten verschieben Nach oben verschieben Neue Wiedergabeliste Neue Wiedergabeliste … Nächster Titel Keine Ergebnisse OK Wiedergeben Alle wiedergeben Wiedergabeliste existiert bereits Vorheriger Titel Radiomodus Aus den heruntergeladenen Dateien entfernen Aus der Warteschlange entfernen Wiedergabeliste umbenennen Umbenennen … Gespeichert Nach Inhalten suchen … Durchmischen Titel Wiedergabe/Pause Videos Interpret anzeigen Lautstärke Ihre Bibliothek ist leer Eigene Wiedergabelisten Jürgen Benvenuti <gastornis@posteo.org>, 2023 